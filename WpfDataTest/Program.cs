﻿using System;
using WpfTest_Shop;
using System.Collections.Generic;
using System.Linq;

namespace Projet3_DataTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new ShopContext())
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                var france = new Country
                {
                    Name = "France",
                };
                var allemagne = new Country()
                {
                    Name = "Allemagne"
                };

                var grandEst = new District()
                {
                    Name = "Grand Est",
                    Country = france,
                };

                var bfc = new District()
                {
                    Name = "Bourgogne Franche-Comté",
                    Country = france,
                };

                var moselle = new County()
                {
                    Name = "Moselle",
                    District = grandEst,
                };
                var basRhin = new County()
                {
                    Name = "Bas-Rhin",
                    District = grandEst,
                };
                var vosges = new County()
                {
                    Name = "Vosges",
                    District = grandEst,
                };

                var metz = new City()
                {
                    Name = "Metz",
                    County = moselle,
                };

                var zipcode1 = new ZipCode()
                {
                    ZipCodeNumber = "57000",
                    City = metz,
                };

                metz.ZipCodes.Add(zipcode1);

                var strasbourg = new City()
                {
                    Name = "Strasbourg",
                    County = basRhin,
                };

                var zipcode2 = new ZipCode()
                {
                    ZipCodeNumber = "67000",
                    City = strasbourg,
                };

                strasbourg.ZipCodes.Add(zipcode2);

                var gerardmer = new City()
                {
                    Name = "Gérardmer",
                    County = vosges,
                };

                var zipcode3 = new ZipCode()
                {
                    ZipCodeNumber = "88400",
                    City = gerardmer,
                };

                gerardmer.ZipCodes.Add(zipcode3);

                List<City> cityList = new List<City>();
                cityList.Add(metz);
                cityList.Add(strasbourg);
                cityList.Add(gerardmer);

                for(int i=0; i< cityList.Count(); i++)
                {
                    cityList[i].Shops = GenerateShop(cityList[i], 5);

                }

                List<Shop> manyShops = new List<Shop>();

                foreach (City city in cityList)
                {
                    if (city.Shops != null)
                    {
                        manyShops.AddRange(city.Shops);
                    }
                }

                var manyPersons = (from i in Enumerable.Range(0, 5)
                                   select new Person { Name = "John" + i, Password = "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4" }).ToList();
                context.AddRange(manyPersons);

                var newPersons = (from i in Enumerable.Range(0, 5)
                                  select new Person { Name = "Johnette" + i, Password = "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4" }).ToList();
                context.AddRange(newPersons);

                var editor1 = GenerateEditor("Faton");
                var editor2 = GenerateEditor("Link Digital Spirit");

                List<Edition> editionlist = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist.Add(newEdition);
                }

                List<Edition> editionlist1 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist1.Add(newEdition);
                }

                List<Edition> editionlist2 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist2.Add(newEdition);
                }
                List<Edition> editionlist3 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist3.Add(newEdition);
                }
                List<Edition> editionlist4 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist4.Add(newEdition);
                }

                List<Edition> editionlist5 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist5.Add(newEdition);
                }

                List<Edition> editionlist6 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist6.Add(newEdition);
                }

                List<Edition> editionlist7 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist7.Add(newEdition);
                }
                List<Edition> editionlist8 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist8.Add(newEdition);
                }
                List<Edition> editionlist9 = new List<Edition>();
                for (int i = 1; i <= 3; i++)
                {
                    Edition newEdition = new Edition { EditionNumber = i, PublicationDate = DateTime.Now };
                    editionlist9.Add(newEdition);
                }

                List<Magazine> magazines = GenerateMagazine(10);

                for (int i = 0; i < magazines.Count; i++)
                {
                    if (i % 2 == 0)
                    {
                        magazines[i].Editor = editor1;
                        magazines[i].PeriodicityDays = 7;
                    }
                    else
                    {
                        magazines[i].Editor = editor2;
                        magazines[i].PeriodicityDays = 14;
                    }
                }

                magazines[0].Editions = editionlist;
                magazines[1].Editions = editionlist1;
                magazines[2].Editions = editionlist2;
                magazines[3].Editions = editionlist3;
                magazines[4].Editions = editionlist4;
                magazines[5].Editions = editionlist5;
                magazines[6].Editions = editionlist6;
                magazines[7].Editions = editionlist7;
                magazines[8].Editions = editionlist8;
                magazines[9].Editions = editionlist9;

                List<Order> manyOrders = GenerateOrder(3);

                List<ShopMagazine> manyShopMagazines = AssociatedShopswithMagazines(manyShops, magazines, 10);
                List<PersonShop> manyPersonShop = AssociatedPersonsWithShops(manyPersons, manyShops);
                List<MagazineOrder> magazineOrdersList = AssociationMagazineWithOrder(magazines, manyOrders);

                context.AddRange(magazines);
                context.AddRange(manyShopMagazines);
                context.AddRange(manyPersonShop);
                context.AddRange(magazineOrdersList);                
                List<OrderShop> orderShopsList = AssociationShopWithOrder(manyShops, manyOrders);

                context.AddRange(magazines);
                context.AddRange(manyShopMagazines);
                context.AddRange(manyPersonShop);
                context.AddRange(magazineOrdersList);
                context.AddRange(magazineOrdersList);
                context.AddRange(france);
                context.AddRange(allemagne);
                context.AddRange(bfc);
                context.AddRange(orderShopsList);
                context.AddRange(cityList);
                context.SaveChanges();
            }
        }

        static List<Shop> GenerateShop(City city, int numberOfShop)
        {
            List<Shop> shopList = new List<Shop>();

            for (int i = 1; i <= numberOfShop; i++)
            {
                var newShop = new Shop();
                Random random = new Random();
                int numberStreet = random.Next(1, 100);
                newShop.Name = "shop" + i + city.Name;
                newShop.StreetNumber = numberStreet.ToString();
                newShop.StreetName = "street" + i;
                newShop.City = city;
                newShop.ZipCode = city.ZipCodes[0];
                shopList.Add(newShop);
            }
            return shopList;

        }

        static Editor GenerateEditor(string name)
        {
            using (var context = new ShopContext())
            {
                var newEditor = new Editor("Fucking shit");
                newEditor.Name = name;
                return newEditor;
            }
        }

        static List<Magazine> GenerateMagazine(int numberOfMagazine)
        {
            List<Magazine> magazines = new List<Magazine>();
            Random random = new Random();
            for (int i = 1; i <= numberOfMagazine; i++)
            {
                var newMagazines = new Magazine();
                int numberRdm = random.Next(1, 100);
                newMagazines.Name = "Magazine" + (i + numberRdm);
                magazines.Add(newMagazines);

            }
            return magazines;
        }

        static List<Order> GenerateOrder(int numberOfOrder)
        {
            List<Order> orders = new List<Order>();
            for (int i = 1; i <= numberOfOrder; i++)
            {
                var newOrder = new Order();
                newOrder.OrderDate = DateTime.Today.AddDays(i);
                newOrder.DeliveryDate = DateTime.Today.AddDays(i + 2);
                orders.Add(newOrder);
            }
            return orders;
        }

        static List<MagazineOrder> AssociationMagazineWithOrder(List<Magazine> manyMags, List<Order> manyOrders)
        {
            List<MagazineOrder> manyMagazineOrders = new List<MagazineOrder>();

            foreach (var order in manyOrders)
            {
                foreach (var mag in manyMags)
                {
                    var magazineOrder = new MagazineOrder { Order = order, OrderId = order.OrderId, Magazine = mag, MagazineId = mag.MagazineId, Quantity = 10 };
                    manyMagazineOrders.Add(magazineOrder);
                }
            }
            return manyMagazineOrders;

        }

        static List<OrderShop> AssociationShopWithOrder(List<Shop> manyShops, List<Order> manyOrders)
        {
            List<OrderShop> manyOrdersShops = new List<OrderShop>();

            foreach (var order in manyOrders)
            {
                foreach (var shop in manyShops)
                {
                    var OrderShop = new OrderShop { Order = order, OrderId = order.OrderId, Shop = shop, ShopId = shop.ShopId };
                    manyOrdersShops.Add(OrderShop);
                }
            }
            return manyOrdersShops;

        }

        public static List<City> GetCityList(County county)
        {
            using (var context = new ShopContext())
            {
                List<City> cities = new List<City>();

                var citiesList = from c in context.City.AsEnumerable()
                                 where c.County.CountyId == county.CountyId
                                 select c;
                cities.AddRange(citiesList);
                return cities;
            }
        }

        static List<ShopMagazine> AssociatedShopswithMagazines(List<Shop> manyShops, List<Magazine> magazines, int magazinesQuantity)
        {
            List<ShopMagazine> manyShopMagazines = new List<ShopMagazine>();
            foreach (var mag in magazines)
            {
                foreach (var shops in manyShops)
                {
                    var shopsMagazines = new ShopMagazine { MagazineId = mag.MagazineId, Magazine = mag, Shop = shops, ShopId = shops.ShopId };
                    manyShopMagazines.Add(shopsMagazines);
                }
            }
            return manyShopMagazines;
        }

        static List<PersonShop> AssociatedPersonsWithShops(List<Person> manyPersons, List<Shop> manyShops)
        {
            List<PersonShop> manyPersonShop = new List<PersonShop>();
            foreach (var shop in manyShops)
            {
                foreach (var person in manyPersons)
                {
                    var personShop = new PersonShop { PersonId = person.PersonId, Person = person, Shop = shop, ShopId = shop.ShopId };
                    manyPersonShop.Add(personShop);
                }
            }
            return manyPersonShop;
        }

    }
}


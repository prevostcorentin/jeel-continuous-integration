﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WpfTest_Shop
{
    public static class DisplayInformation
    {
        public static List<Shop> DisplayDefaultShop()
        {
            using (var context = new ShopContext())
            {
                var defaultList = (from s in context.Shops
                                   select s).ToList();
                return defaultList;
            }
        }

        public static Shop GetShopByName(string shopName)
        {
            using (var context = new ShopContext())
            {
                var shop = (from s in context.Set<Shop>()
                            where s.Name == shopName
                            select s).FirstOrDefault();
                return shop;
            }
        }

        public static List<string> GetDefaultShopNameList()
        {
            using (var context = new ShopContext())
            {
                var shopList = (from s in context.Set<Shop>()
                                select s.Name).ToList();
                return shopList;
            }
        }

        public static List<Magazine> GetAllMagazinesList()
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    select m).ToList();
                return magazineList;
            }
        }

        public static List<string> GetDefaultMagazineList()
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    select m.Name).ToList();
                return magazineList;
            }
        }

        public static List<Magazine> GetMagazinesList()
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    select m).ToList();
                return magazineList;
            }
        }

        public static Magazine GetMagazineByName(string name)
        {
            using (var context = new ShopContext())
            {
                var magazine = (from m in context.Magazines
                                where m.Name == name
                                select m).FirstOrDefault();
                return magazine;
            }
        }

        public static List<string> GetMagazinesNameListByShop(Shop shop)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from sm in context.ShopMagazine
                                    join m in context.Magazines
                                    on sm.MagazineId equals m.MagazineId
                                    where sm.ShopId == shop.ShopId
                                    select m.Name).ToList();
                return magazineList;
            }
        }

        public static dynamic GetMagazineInfosByName(Magazine magazine, Shop shop)
        {
            using (var context = new ShopContext())
            {
                var magazineInfos = (from mo in context.MagazineOrder
                                     join m in context.Magazines
                                     on mo.MagazineId equals m.MagazineId
                                     join os in context.OrderShop
                                     on mo.OrderId equals os.OrderId
                                     where mo.MagazineId == magazine.MagazineId && m.MagazineId == magazine.MagazineId
                                     && os.ShopId == shop.ShopId
                                     select new { m.MagazineId, m.Name, mo.Quantity }).ToList();

                MagazineInfos magazineInfosLine = magazineInfos.GroupBy(x => x.MagazineId)
                                                        .Select(s => new MagazineInfos
                                                        {
                                                            Name = s.First().Name,
                                                            Quantity = s.Sum(q => q.Quantity).ToString(),
                                                            MagazineId = s.First().MagazineId.ToString()
                                                        }).FirstOrDefault();
                return magazineInfosLine;
            }
        }


        public static List<Magazine> GetMagazineListByEditor(Editor editor)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    join e in context.Editor
                                    on m.Editor.EditorId equals e.EditorId
                                    where e.Name == editor.Name
                                    select m).ToList();
                return magazineList;
            }
        }

        public static string GetCurrentEditionNumberByEditionList(List<Edition> selectedMagazineEditionList)
        {
            using (var context = new ShopContext())
            {
                var item = selectedMagazineEditionList
                        .Where(e => e.PublicationDate < DateTime.Now)
                        .OrderBy(e => e.PublicationDate)
                        .Last();

                return (item.EditionNumber + 1).ToString();
            }
        }

        public static Edition GetLastEdition(List<Edition> selectedMagazineEditionList)
        {
            using (var context = new ShopContext())
            {
                var item = selectedMagazineEditionList
                        .OrderBy(e => e.PublicationDate)
                        .Last();

                return item;
            }
        }

        public static DateTime GetCurrentEditionPublicationDate(List<Edition> selectedMagazineEditionList)
        {
            using (var context = new ShopContext())
            {
                var item = selectedMagazineEditionList
                        .Where(e => e.PublicationDate <= DateTime.Now)
                        .OrderBy(e => e.PublicationDate)
                        .Last();

                return item.PublicationDate;
            }
        }


        public static List<string> GetEmployeeByShop(Shop shop)
        {
            using (var context = new ShopContext())
            {
                var employeeList = (from ps in context.PersonShop
                                    join p in context.Person
                                    on ps.PersonId equals p.PersonId
                                    where ps.ShopId == shop.ShopId
                                    select p.Name).ToList();
                return employeeList;
            }
        }

        public static List<Magazine> GetMagazinesListByShop(Shop shop)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from sm in context.ShopMagazine
                                    join m in context.Magazines
                                    on sm.MagazineId equals m.MagazineId
                                    where sm.ShopId == shop.ShopId
                                    select m).ToList();
                return magazineList;
            }
        }

        public static List<Magazine> GetMagazinesListByPeriodicity(int periodicity)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    where m.PeriodicityDays == periodicity
                                    select m).ToList();
                return magazineList;
            }
        }

        public static List<Magazine> GetMagazineListByPeriodicityAndShop(int periodicity, Shop shop)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    join sm in context.ShopMagazine
                                    on m.MagazineId equals sm.MagazineId
                                    where m.PeriodicityDays == periodicity && sm.ShopId == shop.ShopId
                                    select m).ToList();
                return magazineList;
            }
        }

        public static List<Magazine> GetMagazineListByEditorAndShop(Editor editor, Shop shop)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    join sm in context.ShopMagazine
                                    on m.MagazineId equals sm.MagazineId
                                    where m.Editor.EditorId == editor.EditorId && sm.Shop.ShopId == shop.ShopId
                                    select m).ToList();
                return magazineList;
            }
        }

        public static List<Magazine> GetMagazineListByEditorAndPeriodicity(Editor editor, int periodicity)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    where m.Editor.EditorId == editor.EditorId && m.PeriodicityDays == periodicity
                                    select m).ToList();
                return magazineList;
            }
        }

        public static List<Magazine> GetMagazineListByEditorAndPeriodicityAndShop(Editor editor, int periodicity, Shop shop)
        {
            using (var context = new ShopContext())
            {
                var magazineList = (from m in context.Magazines
                                    join sm in context.ShopMagazine
                                    on m.MagazineId equals sm.MagazineId
                                    where m.Editor.EditorId == editor.EditorId && sm.Shop.ShopId == shop.ShopId && m.PeriodicityDays == periodicity
                                    select m).ToList();
                return magazineList;
            }
        }

        public static List<Edition> GetEditionListByMagazine(Magazine magazine)
        {
            using (var context = new ShopContext())
            {
                var editionList = (from e in context.Edition  
                                   join m in context.Magazines
                                   on e.Magazine.MagazineId equals m.MagazineId
                                   where e.Magazine.MagazineId == magazine.MagazineId
                                   select e).ToList();
                return editionList;
            }
        }

        public static dynamic GetEditionListAndMagNameByMagazine(Magazine magazine)
        {
            using (var context = new ShopContext())
            {
                var editionList = (from e in context.Edition
                                   join m in context.Magazines
                                   on e.Magazine.MagazineId equals m.MagazineId
                                   where e.Magazine.MagazineId == magazine.MagazineId
                                   select new { e.EditionId, e.EditionNumber, e.PublicationDate, e.Magazine.Name }).ToList();
                

                List<EditionInfos> editionInfosLine = editionList.Select(s => new EditionInfos
                                                                {
                                                                    EditionId = s.EditionId.ToString(),
                                                                    EditionNumber = s.EditionNumber.ToString(),
                                                                    PublicationDate = s.PublicationDate.ToString("MM/dd/yyyy"),
                                                                    MagazineName = s.Name.ToString(),
                                                                }).ToList();
                return editionInfosLine;
            }
        }


        public static List<Edition> GetEditionList()
        {
            using (var context = new ShopContext())
            {
                var editionList = (from ed in context.Edition
                                   select ed).ToList();
                return editionList;
            }
        }

        public static Magazine GetMagazineByEditionInfos(EditionInfos editionInfos)
        {
            using (var context = new ShopContext())
            {
                var magazine = (from ed in context.Edition
                                join m in context.Magazines
                                on ed.Magazine.MagazineId equals m.MagazineId
                                where ed.Magazine.Name == editionInfos.MagazineName
                                select m).FirstOrDefault();
                return magazine;
            }
        }


        public static List<Editor> GetEditors()
        {
            using (var context = new ShopContext())
            {
                var editorsList = (from ed in context.Editor
                                   select ed).ToList();
                return editorsList;
            }
        }

        public static Editor GetEditorByMagazine(Magazine selectedMagazine)
        {
            using (var context = new ShopContext())
            {
                var editor = (from e in context.Editor
                              where e.Magazines.Contains(selectedMagazine)
                              select e).FirstOrDefault();
                return editor;
            }
        }

        public static Editor GetEditorByName(string editorName)
        {
            using (var context = new ShopContext())
            {
                var editor = (from ed in context.Editor
                              where ed.Name == editorName
                              select ed).FirstOrDefault();
                return editor;
            }
        }

        public static List<Edition> GetAllEditionsOfAMagazine(Magazine mag)
        {
            using (var context = new ShopContext())
            {
                var editionList = (from ed in context.Edition
                                   where ed.Magazine.MagazineId == mag.MagazineId
                                   select ed).ToList();
                return editionList;
            }
        }

        public static List<OrderAutomatic> GetAllOrderAutomatic()
        {
            using (var context = new ShopContext())
            {
                var orderList = (from oa in context.OrderAutomatics
                                 select oa).ToList();
                return orderList;
            }
        }

        public static List<OrderAutomatic> GetAllOrderAutomaticFromMagazine(Magazine mag)
        {
            using (var context = new ShopContext())
            {
                var orderList = (from oa in context.OrderAutomatics
                                 where oa.Magazine.MagazineId == mag.MagazineId
                                 select oa).ToList();
                return orderList;
            }
        }

        public static Shop GetShopByOrderAuto(OrderAutomatic orderAutomatic)
        {
            using (var context = new ShopContext())
            {
                var shop = (from s in context.Shops
                            where s.OrderAutomatics.Contains(orderAutomatic)
                            select s).FirstOrDefault();
                return shop;
            }
        }

        public static List<OrderAutomatic> GetAllOrderAutomaticFromAMagazine(Magazine mag)
        {
            using (var context = new ShopContext())
            {
                var orderList = (from oa in context.OrderAutomatics
                                 where oa.Magazine.MagazineId == mag.MagazineId
                                 select oa).ToList();
                return orderList;
            }
        }

        public static List<Order> GetOrdersByShop(Shop shop)
        {
            using (var context = new ShopContext())
            {
                var ordersList = (from o in context.Order
                                  join os in context.OrderShop
                                  on o.OrderId equals os.OrderId
                                  where os.ShopId == shop.ShopId
                                  select o).ToList();
                return ordersList;
            }
        }

        public static List<MagazineOrder> GetMagazineOrdersByMagazine(Magazine magazine)
        {
            using (var context = new ShopContext())
            {
                var ordersList = (from m in context.Magazines
                                  join mo in context.MagazineOrder
                                  on m.MagazineId equals mo.MagazineId
                                  where m.MagazineId == magazine.MagazineId
                                  select mo).ToList();
                return ordersList;
            }
        }

        public static List<MagazineOrder> GetMagazineOrderByShop(Shop shop)
        {
            using (var context = new ShopContext())
            {
                var orders = (from os in context.OrderShop
                              join o in context.Order
                              on os.OrderId equals o.OrderId
                              join mo in context.MagazineOrder
                              on os.OrderId equals mo.OrderId
                              where os.ShopId == shop.ShopId && o.OrderDate==DateTime.Today
                              select mo).ToList();

                return orders;
            }
        }

        public static List<MagazineOrder> GetMagazineOrder()
        {
            using (var context = new ShopContext())
            {
                var orders = (from o in context.Order
                              join mo in context.MagazineOrder
                              on o.OrderId equals mo.OrderId
                              where o.OrderDate == DateTime.Today
                              select mo).ToList();

                return orders;
            }
        }
    }
}
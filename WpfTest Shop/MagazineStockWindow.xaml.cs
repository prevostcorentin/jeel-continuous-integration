﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class MagazineStockWindow : Window
    {
        public MagazineStockWindow()
        {
            InitializeComponent();
            List<string> shopList = DisplayInformation.GetDefaultShopNameList();
            ShopListComboBox.ItemsSource = shopList;
            ShopListComboBox.SelectionChanged += new SelectionChangedEventHandler(ShopListComboBox_SelectionChanged);
        }

        private void OrderNextEditionBtn_Click(object sender, RoutedEventArgs e)
        {
            OrderNextEditionWindows orderNextEditionWindows = new OrderNextEditionWindows(this);
            orderNextEditionWindows.Owner = this;
            orderNextEditionWindows.Show();
            orderNextEditionWindows.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null && item.IsSelected)
            {
                EditMagazineWindow editMagazineWindow = new EditMagazineWindow(this);
                editMagazineWindow.Show();
            }
        }

        private void Reset_btn_Click(object sender, RoutedEventArgs e)
        {
            List<string> shopList = DisplayInformation.GetDefaultShopNameList();
            ShopListComboBox.ItemsSource = shopList;
            ShopListComboBox.Text = String.Empty;
            MagazineListView.ItemsSource = String.Empty;
        }

        private void Cancel_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ShopListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ShopListComboBox.SelectedItem != null)
            {
                string selectedShopName = ShopListComboBox.SelectedItem.ToString();
                Shop selectedShop = DisplayInformation.GetShopByName(selectedShopName);
                List<Magazine> magazineList = DisplayInformation.GetMagazinesListByShop(selectedShop);
                List<MagazineInfos> magInfoList = GetMagazineInfoByMagazine(magazineList, selectedShop);
                MagazineListView.ItemsSource = magInfoList;
            }
        }

        private List<MagazineInfos> GetMagazineInfoByMagazine(List<Magazine> magList, Shop selectedShop)
        {
            List<MagazineInfos> magInfoList = new List<MagazineInfos>();
            foreach (Magazine mag in magList)
            {
                MagazineInfos info = DisplayInformation.GetMagazineInfosByName(mag, selectedShop);
                magInfoList.Add(info);
            }
            return magInfoList;
        }
    }
}

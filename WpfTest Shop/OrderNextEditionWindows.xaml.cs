﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class OrderNextEditionWindows : Window
    {
        public Shop SelectedShop { get; set; }
        public List<Edition> SelectedMagazineEditionList { get; set; }

        public OrderNextEditionWindows(MagazineStockWindow owner)
        {
            InitializeComponent();
            this.Owner = owner;
            DataContext = owner.MagazineListView.SelectedItem;
            MagazineInfos selectedMagInfo = (MagazineInfos)DataContext;
            string selectedMagazineName = selectedMagInfo.Name;
            Magazine selectedMagazine = DisplayInformation.GetMagazineByName(selectedMagazineName);
            SelectedMagazineEditionList = DisplayInformation.GetEditionListByMagazine(selectedMagazine);
            EditionNumberTextBlock.Text = DisplayInformation.GetCurrentEditionNumberByEditionList(SelectedMagazineEditionList);
            SelectedShop = DisplayInformation.GetShopByName(owner.ShopListComboBox.SelectedItem.ToString());
        }

        private void BtnValidate_Click(object sender, RoutedEventArgs e)
        {
            if(QuantityTextBox.Text != "")
            {
                int currentItemQuantity = Convert.ToInt32(QuantityTextBox.Text);

                if (currentItemQuantity > 0 && currentItemQuantity <= 200)
                {
                    using (var context = new ShopContext())
                    {
                        string magazineName = MagazineNameTextBlock.Text;
                        Magazine magazine = DisplayInformation.GetMagazineByName(magazineName);
                        Order nextEditionOrder = new Order()
                        {
                            OrderDate = DateTime.Now,
                        };

                        List<MagazineOrder> newMagOrdList = new List<MagazineOrder>();
                        MagazineOrder newMagazineOrder = new MagazineOrder()
                        {
                            Magazine = magazine,
                            Order = nextEditionOrder,
                            Quantity = Convert.ToInt32(QuantityTextBox.Text)
                        };
                        newMagOrdList.Add(newMagazineOrder);
                        nextEditionOrder.ManyMagazineOrders = newMagOrdList;

                        List<OrderShop> newOrderShopList = new List<OrderShop>();
                        OrderShop newOrderShop = new OrderShop()
                        {
                            Order = nextEditionOrder,
                            Shop = SelectedShop
                        };
                        newOrderShopList.Add(newOrderShop);
                        nextEditionOrder.ManyOrderShops = newOrderShopList;
                        int Days = magazine.PeriodicityDays;
                        DateTime currentEditionPublicationDate = DisplayInformation.GetCurrentEditionPublicationDate(SelectedMagazineEditionList);
                        DateTime nextEditionPublicationDate = currentEditionPublicationDate.AddDays(Days);
                        nextEditionOrder.DeliveryDate = nextEditionPublicationDate;

                        context.Update(nextEditionOrder);
                        context.SaveChanges();

                        MessageBox.Show("The next edition's order is validated");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("The quantity has to be between 1 and 200!");
                }
            }
            else
            {
                MessageBox.Show("You must enter a quantity to validate!");
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}


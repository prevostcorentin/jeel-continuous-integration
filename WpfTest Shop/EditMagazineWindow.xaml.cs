﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class EditMagazineWindow : Window
    {
        public Magazine SelectedMagazine { get; set; }
        public List<Magazine> MagazinesList { get; set; } = DisplayInformation.GetAllMagazinesList();
        public List<Editor> EditorsList { get; set; }
        public List<string> EditorsName { get; set; }

        public EditMagazineWindow()
        {
            InitializeComponent();
            MagazineNameComboBox.SelectionChanged += new SelectionChangedEventHandler(MagazineNameComboBox_SelectionChanged);
            List<string> magazinesName = MagazinesList.Select(n => n.Name).ToList();
            MagazineNameComboBox.ItemsSource = magazinesName;
        }

        public EditMagazineWindow(MagazineFilterWindow owner)
        {
            InitializeComponent();
            this.Owner = owner;
            DataContext = owner.MagazineListView.SelectedItem; 
            SelectedMagazine = (Magazine)DataContext;
            List<Magazine> magList = new List<Magazine>();
            magList.Add(SelectedMagazine);
            MagazineNameComboBox.ItemsSource = magList;
            MagazineNameComboBox.Text = magList[0].Name;
            NewMagazineNameTxtBox.Text = SelectedMagazine.Name;
            string selectedMagazineEditor = DisplayInformation.GetEditorByMagazine(SelectedMagazine).Name;
            EditorComboBox.Text = selectedMagazineEditor;
            List<Editor> editorList = DisplayInformation.GetEditors();
            EditorComboBox.ItemsSource = editorList.Select(e => e.Name).ToList();
            PeriodicityTxtBox.Text = SelectedMagazine.PeriodicityDays.ToString();
        }

        public EditMagazineWindow(MagazineStockWindow owner)
        {
            InitializeComponent();
            this.Owner = owner;
            DataContext = owner.MagazineListView.SelectedItem;
            MagazineInfos magInfo = (MagazineInfos)DataContext; 
            string selectedMagazineName = magInfo.Name;
            SelectedMagazine = DisplayInformation.GetMagazineByName(selectedMagazineName);
            List<Magazine> magList = new List<Magazine>();
            magList.Add(SelectedMagazine);
            NewMagazineNameTxtBox.Text = SelectedMagazine.Name;
            MagazineNameComboBox.ItemsSource = magList;
            MagazineNameComboBox.Text = magList[0].Name;
            string selectedMagazineEditor = DisplayInformation.GetEditorByMagazine(SelectedMagazine).Name;
            EditorComboBox.Text = selectedMagazineEditor;
            List<Editor> editorList = DisplayInformation.GetEditors();
            EditorComboBox.ItemsSource = editorList.Select(e => e.Name).ToList();
            PeriodicityTxtBox.Text = SelectedMagazine.PeriodicityDays.ToString();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {              
                if (MagazineNameComboBox.SelectedItem != null && NewMagazineNameTxtBox.Text != String.Empty && EditorComboBox.Text != String.Empty && PeriodicityTxtBox.Text != String.Empty && PeriodicityTxtBox.Text != "0")
                {
                    SelectedMagazine = DisplayInformation.GetMagazineByName(MagazineNameComboBox.Text);
                    string newMagazineName;
                    if (NewMagazineNameTxtBox.Text != String.Empty)
                    {
                        newMagazineName = NewMagazineNameTxtBox.Text;
                    }
                    else
                    {
                        newMagazineName = MagazineNameComboBox.Text;
                    }
                    
                    SelectedMagazine.Name = newMagazineName;
                    SelectedMagazine.PeriodicityDays = Convert.ToInt32(PeriodicityTxtBox.Text);
                    List<Editor> editorList = DisplayInformation.GetEditors();
                    List<string> editorNameList = editorList.Select(n => n.Name).ToList();
                    Editor selectedEditor = DisplayInformation.GetEditorByName(EditorComboBox.Text);
                    if (editorNameList.Contains(EditorComboBox.Text))
                    {
                        SelectedMagazine.Editor = selectedEditor;
                    }
                    else
                    {
                        Editor newEditor = new Editor(EditorComboBox.Text);
                        List<Magazine> magazineListOfEditor = new List<Magazine>();
                        SelectedMagazine.Editor = newEditor;
                        magazineListOfEditor.Add(SelectedMagazine);
                        newEditor.Magazines = magazineListOfEditor;
                    }

                    context.Update(SelectedMagazine);
                    context.SaveChanges();
                    MessageBox.Show("Modifications of " + SelectedMagazine.Name + " are saved");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You must fill every field correctly.");
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void MagazineNameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MagazineNameComboBox.SelectedItem != null)
            {
                string selectedMagazineName = MagazineNameComboBox.SelectedItem.ToString();
                NewMagazineNameTxtBox.Text = selectedMagazineName;
                Magazine selectedMagazine = MagazinesList.FirstOrDefault(n => n.Name == selectedMagazineName);
                Editor selectedMagazineEditor = DisplayInformation.GetEditorByMagazine(selectedMagazine);
                EditorComboBox.Text = selectedMagazineEditor.Name;
                PeriodicityTxtBox.Text = selectedMagazine.PeriodicityDays.ToString();

                EditorsList = DisplayInformation.GetEditors();
                EditorsName = EditorsList.Select(n => n.Name).ToList();
                EditorComboBox.ItemsSource = EditorsName;
            }
        }
    }
}

               
                
 
       
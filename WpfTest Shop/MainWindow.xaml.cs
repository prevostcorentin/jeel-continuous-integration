﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ListViewItem = System.Windows.Controls.ListViewItem;

namespace WpfTest_Shop
{
    public partial class MainWindow : Window
    {
        public List<City> Cities { get; set; }
        public List<County> Counties { get; set; }
        public List<District> Districts { get; set; }
        public List<Country> Countries { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            List<Shop> shopList = DisplayInformation.DisplayDefaultShop();
            ShopListView.ItemsSource = shopList;
            FilterNameComboBox.SelectionChanged += new SelectionChangedEventHandler(FilterNameComboBox_SelectionChanged);
            Cities = ShopInformation.GetCities();
            Counties = ShopInformation.GetCounties();
            Districts = ShopInformation.GetDistricts();
            Countries = ShopInformation.GetCountries();
        }



        private void FilterNameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var currentSelectedIndex = FilterNameComboBox.SelectedIndex;

            if (currentSelectedIndex == 3)
            {
                List<string> citiesName = Cities.Select(c => c.Name).ToList();
                InfoFilterComboBox.ItemsSource = citiesName;
            }
            else if (currentSelectedIndex == 2)
            {
                List<string> countiesName = Counties.Select(c => c.Name).ToList();
                InfoFilterComboBox.ItemsSource = countiesName;
            }
            else if (currentSelectedIndex==1)
            {
                var districtsName = Districts.Select(d => d.Name).ToList();
                InfoFilterComboBox.ItemsSource = districtsName;
            }
            else if (currentSelectedIndex == 0)
            {
                var coutriesName = Countries.Select(c => c.Name).ToList();
                InfoFilterComboBox.ItemsSource = coutriesName;
            }
        }

        private void Research_btn_Click(object sender, RoutedEventArgs e)
        {
            List < string > citiesName = Cities.Select(c => c.Name).ToList();
            List<string> countiesName = Counties.Select(c => c.Name).ToList();
            List<string> districtsName = Districts.Select(c => c.Name).ToList();
            List<string> coutriesName = Countries.Select(c => c.Name).ToList();

            if (FilterNameComboBox.Text == "City" && citiesName.Contains(InfoFilterComboBox.Text))
            {
                List<Shop> shopFilterByCity = Filter.FilterByCity(InfoFilterComboBox.Text);
                ShopListView.ItemsSource = shopFilterByCity;
            }
            else if (FilterNameComboBox.Text == "County" && countiesName.Contains(InfoFilterComboBox.Text))
            {
                List<Shop> shopFilterByCounty = Filter.FilterByCounty(InfoFilterComboBox.Text);
                ShopListView.ItemsSource = shopFilterByCounty;
            }
            else if (FilterNameComboBox.Text == "District" && districtsName.Contains(InfoFilterComboBox.Text))
            {
                List<Shop> shopFilterByDistrict = Filter.FilterByDistrict(InfoFilterComboBox.Text);
                ShopListView.ItemsSource = shopFilterByDistrict;
            }
            else if (FilterNameComboBox.Text == "Country" && coutriesName.Contains(InfoFilterComboBox.Text))
            {
                List<Shop> shopFilterByCountry = Filter.FilterByCountry(InfoFilterComboBox.Text);
                ShopListView.ItemsSource = shopFilterByCountry;
            }
            else
            {
                System.Windows.MessageBox.Show("This location doesn't exist");
            }
        }

        private void Reset_btn_Click(object sender, RoutedEventArgs e)
        {
            List<Shop> shopList = DisplayInformation.DisplayDefaultShop();
            ShopListView.ItemsSource = shopList;
            InfoFilterComboBox.Text = String.Empty;
        }

        private void ManualSettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            ManualSettingsWindows manualSettingsWindow = new ManualSettingsWindows(this);
            manualSettingsWindow.Show();
            manualSettingsWindow.Closed += (s, eventarg) =>
            {
                List<Shop> shopList = DisplayInformation.DisplayDefaultShop();
                ShopListView.ItemsSource = shopList;
                this.Activate();            
            };
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;         
            if (item != null && item.IsSelected)
            {
                ShopInfoWindow shopInfoWindow = new ShopInfoWindow(this);
                shopInfoWindow.Show(); 
                shopInfoWindow.Closed += (s, eventarg) =>
                {
                    List<Shop> shopList = DisplayInformation.DisplayDefaultShop();
                    ShopListView.ItemsSource = shopList;
                    this.Activate();
                };
            }            
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Application curApp = Application.Current;
            curApp.Shutdown();
        }
        
        private void AddEditor_Click(object sender, RoutedEventArgs e)
        {
            AddEditorWindow addEditor = new AddEditorWindow();
            addEditor.Owner = this;
            addEditor.Show();
            addEditor.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }

        private void AddShop_Click(object sender, RoutedEventArgs e)
        {
            ShopWindowBase addShopWindow = new AddShopWindow();
            addShopWindow.Owner = this;
            addShopWindow.Show();
            addShopWindow.Closed += (s, eventarg) =>
            {
                List<Shop> shopList = DisplayInformation.DisplayDefaultShop();
                ShopListView.ItemsSource = shopList;
                Cities = ShopInformation.GetCities();
                Counties = ShopInformation.GetCounties();
                Districts = ShopInformation.GetDistricts();
                Countries = ShopInformation.GetCountries();
                this.Activate();
            };
        }

        private void AddMagazine_Click(object sender, RoutedEventArgs e)
        {
            AddMagazineWindow addMag = new AddMagazineWindow();
            addMag.Owner = this;
            addMag.Show();
        }

        private void EditEditor_Click(object sender, RoutedEventArgs e)
        {
            EditEditorWindow editEditor = new EditEditorWindow();
            editEditor.Owner = this;
            editEditor.Show();
            editEditor.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }

        private void EditShop_Click(object sender, RoutedEventArgs e)
        {
            ShopWindowBase editShop = new EditShopWindow();
            editShop.Owner = this;
            editShop.Show();
            editShop.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }

        private void EditMagazine_Click(object sender, RoutedEventArgs e)
        {
            EditMagazineWindow editMagazine = new EditMagazineWindow();
            editMagazine.Owner = this;
            editMagazine.Show();
            editMagazine.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }


        private void DisplayStock_Click(object sender, RoutedEventArgs e)
        {
            MagazineStockWindow displayStock = new MagazineStockWindow();
            displayStock.Owner = this;
            displayStock.Show();
            displayStock.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }

        private void DisplayMagazine_Click(object sender, RoutedEventArgs e)
        {
            MagazineFilterWindow displayMagazine = new MagazineFilterWindow();
            displayMagazine.Owner = this;
            displayMagazine.Show();
            displayMagazine.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }

        private void DisplayEdition_Click(object sender, RoutedEventArgs e)
        {
            MagEditionFilterWindow displayEdition = new MagEditionFilterWindow();
            displayEdition.Owner = this;
            displayEdition.Show();
            displayEdition.Closed += (s, eventarg) =>
            {
                this.Activate();
            };
        }
    }
}

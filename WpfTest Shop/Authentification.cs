﻿using System.Linq;

namespace WpfTest_Shop
{
    public static class Authentification
    {
        public static bool Authentify(Person currentPerson)
        {
            using (var context = new ShopContext())
            { 
                var getNameAndPassword = from p in context.Person
                                         where currentPerson.Name == p.Name && currentPerson.Password == p.Password
                                         select new { p.Name, p.Password };

                if (getNameAndPassword.Count() == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

         public static bool AuthentifyTest(Person currentPerson, Person testPerson)
         {

            if (currentPerson.Name == testPerson.Name && Sha256Tools.GetHash(currentPerson.Password) == Sha256Tools.GetHash(testPerson.Password))
            {
                return true;
            }
            else
            {
                return false;
            }           
         }
    }
}

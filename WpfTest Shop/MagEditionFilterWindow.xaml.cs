﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class MagEditionFilterWindow : Window
    {
        public List<EditionInfos> EditionInfosList { get; set; } = new List<EditionInfos>();
        public List<Magazine> MagazineList { get; set; } = new List<Magazine>();
        public MagEditionFilterWindow()
        {
            InitializeComponent();
            MagazineList = DisplayInformation.GetMagazinesList();
            List<string> magazineListName = MagazineList.Select(n => n.Name).ToList();
            MagazineBox.ItemsSource = magazineListName;
        }

        private void Research_Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayEditionListWithFilter();
        }

        private void DisplayEditionListWithFilter()
        {
            if (MagazineBox.SelectedItem != null && datePicker1.SelectedDate == null)
            {
                List<EditionInfos> editionList = new List<EditionInfos>();
               
                foreach (String item in MagazineBox.SelectedItems)
                {
                    Magazine magazine = MagazineList.FirstOrDefault(m => m.Name == item);

                    var EditionListWithMagName = DisplayInformation.GetEditionListAndMagNameByMagazine(magazine);
                    editionList.AddRange(EditionListWithMagName); 
                }
                EditionInfosList = editionList;
                EditionslistView.ItemsSource = EditionInfosList;
            }
            else if (datePicker1.SelectedDate != null && MagazineBox.SelectedItem == null)
            {
                List<EditionInfos> editionList = new List<EditionInfos>();

                DateTime releaseDate = datePicker1.SelectedDate.Value.Date;
                foreach (Magazine mag in MagazineList)
                {
                    var EditionListWithMagName = DisplayInformation.GetEditionListAndMagNameByMagazine(mag);
                    editionList.AddRange(EditionListWithMagName);
                }
                List<EditionInfos> editionsInfosFilterByDate = editionList.Where(x => x.PublicationDate == releaseDate.Date.ToString("MM/dd/yyyy")).ToList();
                EditionInfosList = editionsInfosFilterByDate;
                EditionslistView.ItemsSource = EditionInfosList;
            }            

            else if (MagazineBox.SelectedItems != null && datePicker1.SelectedDate != null)
            {               
                List<EditionInfos> editionList = new List<EditionInfos>();

                foreach (String item in MagazineBox.SelectedItems)
                {
                    Magazine magazine = MagazineList.FirstOrDefault(m => m.Name == item);
                    var EditionListWithMagName = DisplayInformation.GetEditionListAndMagNameByMagazine(magazine);
                    
                    editionList.AddRange(EditionListWithMagName);
                }
                DateTime releaseDate = datePicker1.SelectedDate.Value.Date;
                List<EditionInfos> editionsInfoListFilterByDateAndMag = editionList.Where(p => p.PublicationDate == releaseDate.Date.ToString("MM/dd/yyyy")).ToList();
                EditionInfosList = editionsInfoListFilterByDateAndMag;
                EditionslistView.ItemsSource = EditionInfosList; 
            }       
        }

        private void Reset_Button_Click(object sender, RoutedEventArgs e)
        {
            datePicker1.SelectedDate = null;
            MagazineBox.SelectedItem = null;
            EditionslistView.ItemsSource = String.Empty;
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;

            if (item != null && item.IsSelected)
            {
                EditEditionWindow editEditionWindow = new EditEditionWindow(this);
                editEditionWindow.Show();
                editEditionWindow.Closed += (s, eventarg) =>
                {
                    EditionListDataBinding();
                    this.Activate();
                };
            }
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddEdition_Button_Click(object sender, RoutedEventArgs e)
        {
            AddEditionWindow addEditionWindow = new AddEditionWindow();
            addEditionWindow.Show();
            addEditionWindow.Closed += (s, eventarg) =>
            {
                EditionListDataBinding();
                this.Activate();
                
            };
        }

        private void EditionListDataBinding()
        {
            EditionInfosList = null;
            DisplayEditionListWithFilter();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class AddMagazineWindow : Window
    {
        public List<Editor> EditorsList { get; set; }

        public AddMagazineWindow()
        {
            InitializeComponent();
            EditorsList = DisplayInformation.GetEditors();
            List<string> editorName = EditorsList.Select(n => n.Name).ToList();
            EditorComboBox.ItemsSource = editorName;
            EditionTxtBlock.Text = "1";
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                if (MagazineNameTxtBox.Text != String.Empty && EditorComboBox.SelectedItem != null && PeriodicityTxtBox.Text != String.Empty && EditionTxtBlock.Text != String.Empty)
                {
                    if (Convert.ToInt32(PeriodicityTxtBox.Text) > 0 && Convert.ToInt32(EditionTxtBlock.Text) > 0)
                    {
                        string editorName = EditorComboBox.Text;
                        Editor editor = EditorsList.FirstOrDefault(n => n.Name == editorName);
                        Edition edition = new Edition(Convert.ToInt32(EditionTxtBlock.Text));
                        Magazine addedMagazine = new Magazine(MagazineNameTxtBox.Text);
                        addedMagazine.Editor = editor;
                        addedMagazine.PeriodicityDays = Convert.ToInt32(PeriodicityTxtBox.Text);
                        edition.Magazine = addedMagazine;
                        edition.PublicationDate = DateTime.Now;
                        List<Edition> EditionsOfMagazine = new List<Edition>();
                        EditionsOfMagazine.Add(edition);
                        addedMagazine.Editions = EditionsOfMagazine;

                        context.Update(addedMagazine);
                        context.SaveChanges();
                        MessageBox.Show("New magazine saved");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("The Periodicity must be a positive number not null, so as the Edition number.");
                    }
                }
                else
                {
                    MessageBox.Show("You must fill every field correctly.");
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}  


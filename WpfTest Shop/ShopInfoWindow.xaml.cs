﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WpfTest_Shop
{
    public partial class ShopInfoWindow : Window
    {
        public ShopInfoWindow(MainWindow owner)
        {
            InitializeComponent();
            this.Owner = owner;
            DataContext = owner.ShopListView.SelectedItem;

            string city = ShopInformation.GetCityNameByShop((Shop)DataContext);

            CityTextBlock.Text = city;
            string zc = ShopInformation.GetZipcodeNumberByShop((Shop)DataContext);
            ZipCodeTextBlock.Text = zc;

            var county = ShopInformation.GetCountyFromCityName(CityTextBlock.Text);
            CountyTextBlock.Text = county.Name;

            var district = ShopInformation.GetDistrictFromCountyName(county.Name);
            DistrictTextBlock.Text = district.Name;

            var country = ShopInformation.GetCountryFromDistrictName(district.Name);
            CountryTextBlock.Text = country.Name;

            EmployeeBox.ItemsSource = DisplayInformation.GetEmployeeByShop((Shop)DataContext);

            var mags = DisplayInformation.GetMagazinesListByShop((Shop)DataContext);
            var magsName = mags.Select(n => n.Name).ToList();
            MagazineBox.ItemsSource = magsName;
        }

        private void ManualSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            ManualSettingsWindows manualSettingsWindow = new ManualSettingsWindows(this);
            manualSettingsWindow.Show();
            manualSettingsWindow.Closed += (s, eventarg) =>
            {
                MagazineBox.ItemsSource = DisplayInformation.GetMagazinesListByShop((Shop)DataContext);
                this.Close();
            };
        }

        private void DeleteShopButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                Shop selectedShop = (Shop)DataContext;
                List<Order> ordersFromShop = DisplayInformation.GetOrdersByShop(selectedShop);
                context.Remove((Shop)DataContext);
                context.RemoveRange(ordersFromShop);
                context.SaveChanges();
            }
            MessageBox.Show("Shop deleted, as well as the orders linked to it!");
            this.Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

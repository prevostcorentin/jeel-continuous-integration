﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WpfTest_Shop
{
    public static class Creation
    {
        public static Edition CreateEdition(Magazine magazine)
        {
            using (var context = new ShopContext())
            {
                List<Edition> editions = DisplayInformation.GetEditionListByMagazine(magazine);
                Edition lastEdition = DisplayInformation.GetLastEdition(editions);
                DateTime lastParution = lastEdition.PublicationDate;
                int lastEditionNumber = lastEdition.EditionNumber;

                var newEdition = new Edition()
                {
                    EditionNumber = lastEditionNumber + 1,
                    PublicationDate = lastParution.AddDays(magazine.PeriodicityDays),
                    Magazine = magazine
                };
                return newEdition;            
            }
        }

        public static Order CreateOrder(Edition lastEdition, Magazine magazine)
        {
            var editionToOrder = DisplayInformation.GetAllOrderAutomaticFromMagazine(magazine);
            var publicationNewEdition = lastEdition.PublicationDate.AddDays(magazine.PeriodicityDays);
            var isEditionToOrder = editionToOrder.Where(e => e.StartDate <= publicationNewEdition && publicationNewEdition <= e.EndDate).ToList();
            var order = new Order();
            if (isEditionToOrder.Count > 0)
            {
                var orderAutoForEdition = isEditionToOrder.FirstOrDefault();
                var shopOrderer = DisplayInformation.GetShopByOrderAuto(orderAutoForEdition);

                order.DeliveryDate = publicationNewEdition;
                order.OrderDate = DateTime.Now;

                OrderShop orderShop = Service.AssociatedOneOrdertoOneShop(order, shopOrderer);
                order.ManyOrderShops.Add(orderShop);
                MagazineOrder magazineOrder = Service.AssociatedOneMagToOneOrder(magazine, order, orderAutoForEdition.Quantity);
                order.ManyMagazineOrders.Add(magazineOrder);

                return order;

            }
            else
            { return null; }

        }
    }
}

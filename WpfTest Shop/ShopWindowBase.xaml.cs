﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using WpfTest_Shop.Repository;

namespace WpfTest_Shop
{
    public abstract partial class ShopWindowBase : Window
    {
        public List<Person> UnemployedPersonsList;
        public List<string> MyDataMagazineList { get; set; }
        public List<City> Cities { get; set; }
        public List<County> Counties { get; set; }
        public IEnumerable<Shop> Shops { get; }
        public List<District> Districts { get; set; }
        public List<Country> Countries { get; set; }
        public List<ZipCode> ZipCodes { get; set; }

        private ShopRepository _shopRepository;
        private MagazineRepository _magazineRepository;
        private PersonRepository _personRepository;

        public ShopWindowBase()
        {
            _magazineRepository = new MagazineRepository();
            _personRepository = new PersonRepository();
            _shopRepository = new ShopRepository();
            InitializeComponent();
            Shops = _shopRepository.FindAll();
            UnemployedPersonsList = _personRepository.FindUnemployedPersons().ToList();
            EmployeeLeftbox.ItemsSource = UnemployedPersonsList.Select(p => p.Name);
            List<Magazine> magazines = DisplayInformation.GetAllMagazinesList();

            MagazineLeftbox.ItemsSource = magazines.Select(n => n.Name).ToList();

            Cities = ShopInformation.GetCities();
            var citiesName = Cities.Select(c => c.Name).ToList();
            CityCombobox.ItemsSource = citiesName;

            Counties = ShopInformation.GetCounties();
            var countiesName = Counties.Select(c => c.Name).ToList();
            CountyCombobox.ItemsSource = countiesName;

            Districts = ShopInformation.GetDistricts();
            var districtsName= Districts.Select(d => d.Name).ToList();
            DistrictCombobox.ItemsSource = districtsName;

            Countries = ShopInformation.GetCountries();
            var countries = Countries.Select(c => c.Name).ToList();
            CountryCombobox.ItemsSource = countries;

            ZipCodes = ShopInformation.GetZipCodes();
            var zpName = ZipCodes.Select(z => z.ZipCodeNumber).ToList();
            ZipCodeComboBox.ItemsSource = zpName.Distinct();

            MyDataMagazineList = magazines.Select(n=>n.Name).ToList();
        }

        private void AddEmployeeButton_Click(object sender, RoutedEventArgs e)
        { 
            if (UnemployedPersonsList != null && EmployeeLeftbox.SelectedItem != null) 
            {
                String personsName = EmployeeLeftbox.SelectedValue.ToString();
                EmployeeRightBox.Items.Add(personsName);
                Person concernedPerson = _personRepository.FindByName(personsName);
                UnemployedPersonsList.Remove(concernedPerson);
                ApplyEmployeeDataBinding();
            }
            else
            {
                MessageBox.Show("You must select an existing employee from the left box to add him/her.");
            }
        }

        private void RemoveEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeeRightBox.SelectedItem != null)
            {
                string personsName = EmployeeRightBox.SelectedValue.ToString();
                Person person = _personRepository.FindByName(personsName);
                UnemployedPersonsList.Add(person);
                EmployeeRightBox.Items.RemoveAt(EmployeeRightBox.Items.IndexOf(EmployeeRightBox.SelectedItem));
                ApplyEmployeeDataBinding();
            }
            else
            {
                MessageBox.Show("You must select an added employee from the right box to remove him/her.");
            }
        }

        private void ApplyEmployeeDataBinding()
        {
            EmployeeLeftbox.ItemsSource = null;
            EmployeeLeftbox.ItemsSource = UnemployedPersonsList;
        }

        protected List<Person> AddedPersons
        {
            get
            {
                var personList = EmployeeRightBox.Items;
                List<Person> addedPerson = new List<Person>();
                foreach (string name in personList)
                {
                    var person = _personRepository.FindByName(name);
                    addedPerson.Add(person);
                }
                return addedPerson;
            }
        }

        private void AddMagazineButton_Click(object sender, RoutedEventArgs e)
        {
            if (MyDataMagazineList != null && MagazineLeftbox.SelectedItem != null &&
                MagazineQuantityTxtBox.Text != "")
            {
                int currentItemQuantity = Convert.ToInt32(MagazineQuantityTxtBox.Text);

                if (currentItemQuantity > 0 && currentItemQuantity <= 200)
                {
                    string currentItemText = MagazineLeftbox.SelectedValue.ToString();
                    int currentItemIndex = MagazineLeftbox.SelectedIndex;
                   
                    MagazineRightbox.Items.Add(currentItemText);
                    MagazineQuantityRightbox.Items.Add(currentItemQuantity);
                    MyDataMagazineList.RemoveAt(currentItemIndex);
                    ApplyMagazineDataBinding();
                    MagazineQuantityTxtBox.Clear();
                }
                else
                {
                    MessageBox.Show("The quantity for a magazine has to be between 1 and 200!");
                }
            }
            else
            {
                MessageBox.Show("You must select a magazine and add a quantity.");
            }
        }

        private void RemoveMagazineButton_Click(object sender, RoutedEventArgs e)
        {
            if (MagazineRightbox.SelectedItem != null)
            {
                string currentItemText = MagazineRightbox.SelectedValue.ToString();
                MyDataMagazineList.Add(currentItemText);
                int indextoremove = MagazineRightbox.Items.IndexOf(MagazineRightbox.SelectedItem);
                MagazineQuantityRightbox.Items.RemoveAt(indextoremove);
                MagazineRightbox.Items.RemoveAt(MagazineRightbox.Items.IndexOf(MagazineRightbox.SelectedItem));
                ApplyMagazineDataBinding();
            }
            else
            {
                MessageBox.Show("You must select an added magazine from the right box to remove it.");
            }
        }

        private void ApplyMagazineDataBinding()
        {
            MagazineLeftbox.ItemsSource = null;
            MagazineLeftbox.ItemsSource = MyDataMagazineList;
        }

        protected List<Magazine> AddedMagazines
        {
            get
            {
                var magazineList = MagazineRightbox.Items;
                List<Magazine> addedMagazines = new List<Magazine>();
                foreach (string name in magazineList)
                {
                    var magazine = _magazineRepository.FindByName(name);
                    addedMagazines.Add(magazine);
                }
                return addedMagazines;
            }
        }

        protected List<int> AddedMagazinesQuantity
        {
            get
            {
                var magazineQuantityList = MagazineQuantityRightbox.Items;
                List<int> addedMagazinesQuantity = new List<int>();
                foreach (int quantity in magazineQuantityList)
                {
                    addedMagazinesQuantity.Add(quantity);
                }
                return addedMagazinesQuantity;
            }
        }

        protected abstract void SaveButton_Click(object sender, RoutedEventArgs e);

        protected bool IsNewZipCode(string cityName, string numberZC)
        {
            var zipcodesNumber = ZipCodes.Select(z => z.ZipCodeNumber).ToList();
            var citiesName = Cities.Select(c => c.Name).ToList();
            if (citiesName.Contains(cityName) && !zipcodesNumber.Contains(numberZC))
            {
                return true;
            }
            else { return false; }
        }

        protected Shop AddShop(List<Magazine> magazines)
        {
            List<Person> persons = AddedPersons;
            var newShop = new Shop()
            {
                Name = ShopNameTxtBox.Text,
                StreetNumber = StreetNumberTxtBox.Text,
                StreetName = StreetNameTxtBox.Text,
            };
            newShop.ManyPersonShops = AssociatedManyPersonToOneShop(persons, newShop);
            newShop.ManyShopMagazines = AssociatedManyMagazineToOneShop(magazines, newShop);
            return newShop;
        }
        
        protected bool IsNewCity (string name)
        {
            var citiesName = Cities.Select(c => c.Name).ToList();

            if (citiesName.Contains(name))
            {
                return false;
            }
            else { return true; }
        }

        protected City CreateNewCity()
        {
            List<string> counties = Counties.Select(c => c.Name).ToList();
            County county = Counties.FirstOrDefault(n => n.Name == CountyCombobox.Text);
            List<string> zipCodes = ZipCodes.Select(z => z.ZipCodeNumber).ToList();

            var city = new City()
            {
                Name = CityCombobox.Text,
            };
            if(!zipCodes.Contains(ZipCodeComboBox.Text))
            {
                var newZC = CreateZipCode();
                city.ZipCodes.Add(newZC);
            }

            if (!counties.Contains(CountyCombobox.Text))
            {
                city.County = CreateCounty();
            }
            else
            {
                city.County = county;
            }

            return city;
        }

        protected ZipCode CreateZipCode()
        {
            var zipCode = new ZipCode()
            {
                ZipCodeNumber = ZipCodeComboBox.Text,
            };

            return zipCode;
        }

        private County CreateCounty()
        {
            var districtsName = Districts.Select(d => d.Name).ToList();
            District district = Districts.FirstOrDefault(n => n.Name == DistrictCombobox.Text);

            var county = new County()
            {
                Name = CountyCombobox.Text,
                
            };
            if(!districtsName.Contains(DistrictCombobox.Text))
            {
                county.District = CreateDistrict();
            }
            else
            {
                county.District = district;
            }

            return county;
        }

        private District CreateDistrict()
        {
            var countries = Countries.Select(c => c.Name).ToList();
            Country country = Countries.FirstOrDefault(n => n.Name == CountryCombobox.Text);
            var district = new District()
            {
                Name = DistrictCombobox.Text,
                
            };
            if(!countries.Contains(CountryCombobox.Text))
            {
                district.Country = CreateCountry();
            }
            else
            {
                district.Country = country;
            }
            return district;
        }

        private Country CreateCountry()
        {
            var country = new Country()
            {
                Name = CountryCombobox.Text,
            };
            return country;
        }

        protected bool IsFormFilled
        {
            get
            {
                if (ShopNameTxtBox.Text != String.Empty && StreetNumberTxtBox.Text != String.Empty && StreetNameTxtBox.Text != String.Empty &&
                        ZipCodeComboBox.Text != String.Empty && CityCombobox.Text != String.Empty && EmployeeRightBox.Items.Count != 0 &&
                        MagazineRightbox.Items.Count != 0 && MagazineQuantityRightbox.Items.Count != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        private List<PersonShop> AssociatedManyPersonToOneShop(List<Person> persons, Shop shop)
        {
            List<PersonShop> personShops = new List<PersonShop>();
            foreach (Person person in persons)
            {
                var personShop = new PersonShop { Person = person, PersonId = person.PersonId, Shop = shop, ShopId = shop.ShopId };
                personShops.Add(personShop);
            }
            return personShops;
        }

        private List<ShopMagazine> AssociatedManyMagazineToOneShop(List<Magazine> magazines, Shop shop)
        {
            List<ShopMagazine> magazineShops = new List<ShopMagazine>();
            for (int i = 0; i < magazines.Count; i++)
            {
                var magazineShop = new ShopMagazine { Magazine = magazines[i], MagazineId = magazines[i].MagazineId, Shop = shop, ShopId = shop.ShopId};
                magazineShops.Add(magazineShop);
            }
            return magazineShops;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

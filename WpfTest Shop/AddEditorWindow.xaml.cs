﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class AddEditorWindow : Window
    {
        public List<Magazine> AddedMagazineList { get; set; } = new List<Magazine>();
        public List<MagazineInfos> InfosMagListview { get; set; } = new List<MagazineInfos>(); 
        public MagazineInfos MagazineInfos { get; set; }
        public List<Edition> EditionsList { get; set; } = new List<Edition>();

        public AddEditorWindow()
        {
            InitializeComponent();   
        }

        private void AddMagazineButton_Click(object sender, RoutedEventArgs e)
        {
            if (MagazineToAdd.Text != String.Empty && PeriodicityTxtBox.Text != String.Empty)
            {
                AddMagazine();
            }
            else
            {
                MessageBox.Show("You must enter all the necessary informations to create a magazine.");
            }            
        }

        private void RemoveMagazineButton_Click(object sender, RoutedEventArgs e)
        {
            if (MagazinesListView.SelectedItem != null)
            {
                EditionsList.RemoveAt(MagazinesListView.Items.IndexOf(MagazinesListView.SelectedItem));
                AddedMagazineList.RemoveAt(MagazinesListView.Items.IndexOf(MagazinesListView.SelectedItem));
                InfosMagListview.RemoveAt(MagazinesListView.Items.IndexOf(MagazinesListView.SelectedItem));
                ApplyMagazineDataBinding();
            }
            else
            {
                MessageBox.Show("Please select a magazine to remove it.");
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                List<Editor> editorsList = DisplayInformation.GetEditors();
                var item = (editorsList.Where(x => x.Name.ToLower() == EditorNameTxtBox.Text.ToLower())).ToList();

                if (item.Count == 0 && EditorNameTxtBox.Text != String.Empty)
                {
                    Editor editor = new Editor(EditorNameTxtBox.Text);
                    editor.Magazines = AddedMagazineList;
                    context.AddRange(EditionsList);
                    context.Add(editor);
                    context.SaveChanges();
                    MessageBox.Show("Editor saved.");
                }
                else
                {
                    MessageBox.Show("This editor already exists, create a new one!");
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void AddMagazine()
        {
            string magName = MagazineToAdd.Text;
            var item = (AddedMagazineList.Where(x => x.Name == MagazineToAdd.Text)).ToList();
            int pariodicityDays = Convert.ToInt32(PeriodicityTxtBox.Text);
            if (item.Count == 0 && magName != null && pariodicityDays > 0 && DatePickerEdition.SelectedDate != null && DatePickerEdition.SelectedDate > DateTime.Today)
            {
                Magazine newMagazine = new Magazine();
                newMagazine.Name = MagazineToAdd.Text;
                newMagazine.PeriodicityDays = pariodicityDays;
                Edition firstEdition = new Edition();
                firstEdition.Magazine = newMagazine;
                firstEdition.EditionNumber = 1;
                firstEdition.PublicationDate = DatePickerEdition.SelectedDate.Value.Date;
                MagazineInfos = new MagazineInfos();
                MagazineInfos.Name = MagazineToAdd.Text;
                MagazineInfos.PeriodicityDaysMagazine = PeriodicityTxtBox.Text;
                MagazineInfos.DateParutionFirstEdition = DatePickerEdition.SelectedDate.Value.Date;
                InfosMagListview.Add(MagazineInfos);
                AddedMagazineList.Add(newMagazine);
                EditionsList.Add(firstEdition);
                ApplyMagazineDataBinding();
            }
            else
            {
                MessageBox.Show("Check the information you have entered : \n\t- all the entries are filled in, \n\t- the magazine doesn't exist, \n\t- if the periodicity is greater than 0,\n\t- check the date (not null, not inferior to today's date).");
            }
        }

        private void ApplyMagazineDataBinding()
        {
            MagazinesListView.ItemsSource = null;
            MagazinesListView.ItemsSource = InfosMagListview;
            PeriodicityTxtBox.Text = String.Empty;
            DatePickerEdition.SelectedDate = null;
            MagazineToAdd.Text = String.Empty;
        }
    }
}

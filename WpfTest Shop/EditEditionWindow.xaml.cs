﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WpfTest_Shop
{
    public partial class EditEditionWindow : Window
    {
        public EditionInfos EditionInfos { get; set; }
        public Magazine Magazine { get; set; }

        public EditEditionWindow(MagEditionFilterWindow owner)
        {
            InitializeComponent();
            this.Owner = owner;
            DataContext = owner.EditionslistView.SelectedItem;
            EditionInfos = (EditionInfos)DataContext;
            Magazine = DisplayInformation.GetMagazineByEditionInfos(EditionInfos);
            MagazineNameTextBlock.Text = EditionInfos.MagazineName;
            EditionTextBlock.Text = EditionInfos.EditionNumber;
            PublicationDateTextBlock.Text = EditionInfos.PublicationDate;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                int selectedEditionNumber = Convert.ToInt32(EditionInfos.EditionNumber);
                List<Edition> editionList = DisplayInformation.GetEditionListByMagazine(Magazine);
                Edition selectedEdition = editionList.FirstOrDefault(x => x.EditionNumber == selectedEditionNumber);
                List<MagazineOrder> selectedMagazineOrdersList = DisplayInformation.GetMagazineOrdersByMagazine(Magazine);

                context.Remove(selectedEdition);
                context.RemoveRange(selectedMagazineOrdersList);
                context.SaveChanges();
                MessageBox.Show("Edition is deleted");
                this.Close();
            }
        }
    }
}

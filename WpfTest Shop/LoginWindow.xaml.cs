﻿using System.Windows;
using System.Windows.Forms;

namespace WpfTest_Shop
{
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void btnEnter_Click(object sender, RoutedEventArgs e)
        {
            Person newPerson = new Person { Name = UserNameTextBox.Text, Password = Sha256Tools.GetHash(PasswordTextBox.Password) };
            if (Authentification.Authentify(newPerson))
            {
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Username or password invalid", "User issue!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }

    }
}

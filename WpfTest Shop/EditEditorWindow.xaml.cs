﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class EditEditorWindow : Window
    {
        public List<Magazine> MagazineListByEditor { get; set; } = new List<Magazine>();
        public List<Editor> EditorList { get; set; } = new List<Editor>();
        public List<Magazine> MagazinesToRemove { get; set; } = new List<Magazine>();
        public EditEditorWindow()
        {
            InitializeComponent();
            EditorList = DisplayInformation.GetEditors();
            List<string> editorListName = EditorList.Select(n => n.Name).ToList();
            EditorListComboBox.ItemsSource = editorListName;
            EditorListComboBox.SelectionChanged += new SelectionChangedEventHandler(EditorListComboBox_SelectionChanged);
        }

        private void RemoveMagazineButton_Click(object sender, RoutedEventArgs e)
        {
            if (MagazineListView.SelectedItem != null)
            {
                RemoveMagazine();
            }
            else
            {
                MessageBox.Show("Please select a magazine to remove it.");
            }
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                context.RemoveRange(MagazinesToRemove);
                Editor editor = EditorList.FirstOrDefault(x => x.Name == EditorListComboBox.Text);
                editor.Name = EditEditorTextBox.Text;
                editor.Magazines = MagazineListByEditor;
                Magazine magazineSelected = MagazinesToRemove.FirstOrDefault();                
                if(magazineSelected != null)
                {
                    List<Edition> editionsToRemove = DisplayInformation.GetEditionListByMagazine(magazineSelected);
                    context.RemoveRange(editionsToRemove);
                }                
                context.Update(editor);
                
                context.SaveChanges();
                MessageBox.Show("Changes have been saved.");
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Update_Btn_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You have update your magazine.");
        }

        private void ApplyMagazineDataBinding()
        {
            MagazineListView.ItemsSource = null;
            MagazineListView.ItemsSource = MagazineListByEditor;
        }

        private void EditorListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String selectedEditorName = EditorListComboBox.SelectedItem.ToString();
            EditEditorTextBox.Text = selectedEditorName;
            Editor editor = EditorList.FirstOrDefault(x => x.Name == selectedEditorName);
            MagazineListByEditor = DisplayInformation.GetMagazineListByEditor(editor);
            MagazineListView.ItemsSource = MagazineListByEditor;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void RemoveMagazine()
        {
            using (var context = new ShopContext())
            {
                if (MagazineListView.SelectedItem != null)
                {
                    Magazine magazineSelected = (Magazine)MagazineListView.SelectedItem;
                    MagazineListByEditor.RemoveAt(MagazineListView.Items.IndexOf(MagazineListView.SelectedItem));
                    ApplyMagazineDataBinding();
                    MagazinesToRemove.Add(magazineSelected);
                }
                else
                {
                    MessageBox.Show("Please select a magazine to remove it.");
                }
            }
        }
    }
}

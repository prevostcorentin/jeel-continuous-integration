﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfTest_Shop
{
    class EditShopWindow : ShopWindowBase
    {
        public List<string> ExistingEmployeeList { get; set; }

        public EditShopWindow() : base()
        {
            SelectShopLabel.Visibility = Visibility.Visible;
            SelectShopComboBox.Visibility = Visibility.Visible;
            SelectShopComboBox.ItemsSource = DisplayInformation.DisplayDefaultShop();
            SelectShopComboBox.SelectionChanged += SelectShopComboBox_SelectionChanged;
            DeleteShopButton.Visibility = Visibility.Visible;
            DeleteShopButton.Click += DeleteShopButton_Click;
        }

        private void DeleteShopButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                context.Remove((Shop)SelectShopComboBox.SelectedItem);
                context.SaveChanges();
            }
            MessageBox.Show("Shop deleted!");
            Close();
        }

        protected override void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                City selectedCity = ShopInformation.GetCityFromName(CityCombobox.Text);
                ZipCode zipcode = ShopInformation.GetZipCodeByNumber(ZipCodeComboBox.Text);

                if (IsFormFilled)
                {
                    Shop editShop = (Shop)SelectShopComboBox.SelectedItem;
                    editShop.Name = ShopNameTxtBox.Text;
                    editShop.StreetNumber = StreetNumberTxtBox.Text;
                    editShop.StreetName = StreetNameTxtBox.Text;
                    editShop.ZipCode = zipcode;

                    if (IsNewCity(CityCombobox.Text))
                    {
                        selectedCity = CreateNewCity();
                        editShop.ZipCode = selectedCity.ZipCodes[0];
                    }
                    if (IsNewZipCode(CityCombobox.Text, ZipCodeComboBox.Text))
                    {
                        zipcode = CreateZipCode();
                        zipcode.City = selectedCity;
                        editShop.ZipCode = zipcode;

                    }

                    if (selectedCity.Shops == null)
                    {
                        selectedCity.Shops = new List<Shop>();
                    }
                    selectedCity.Shops.Add(editShop);

                    List<PersonShop> personsShopToDelete = SelectPersonsShop(SelectShopComboBox.Text);

                    foreach (PersonShop person in personsShopToDelete)
                    {
                        context.Remove(person);
                    }
                    List<Person> persons = AddedPersons;
                    List<PersonShop> personsShop = Service.AssociatedManyPersonToOneShop(persons, editShop);

                    editShop.ManyPersonShops = personsShop;

                    context.Update(selectedCity);
                    context.Update(editShop);
                    context.SaveChanges();
                    MessageBox.Show("Shop Updated");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You must fill every field correctly.");
                }
            }
        }

        private List<PersonShop> SelectPersonsShop(string shopName)
        {
            using (var context = new ShopContext())
            {
                var personsShop = (from ps in context.PersonShop
                                   join s in context.Shops
                                   on ps.ShopId equals s.ShopId
                                   where ps.Shop.Name == shopName
                                   select ps).ToList();
                return personsShop;
            }
        }

        private void SelectShopComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedShop = (Shop)SelectShopComboBox.SelectedItem;

            ShopNameTxtBox.Text = selectedShop.Name;
            StreetNumberTxtBox.Text = selectedShop.StreetNumber;
            StreetNameTxtBox.Text = selectedShop.StreetName;

            string city = ShopInformation.GetCityNameByShop(selectedShop);

            CityCombobox.Text = city;

            string zc = ShopInformation.GetZipcodeNumberByShop(selectedShop);

            ZipCodeComboBox.Text = zc;

            var county = ShopInformation.GetCountyFromCityName(city);
            CountyCombobox.Text = county.Name;

            var district = ShopInformation.GetDistrictFromCountyName(county.Name);
            DistrictCombobox.Text = district.Name;

            var country = ShopInformation.GetCountryFromDistrictName(district.Name);
            CountryCombobox.Text = country.Name;

            ExistingEmployeeList = DisplayInformation.GetEmployeeByShop(selectedShop);
            EmployeeRightBox.ItemsSource = ExistingEmployeeList;
        }

    }
}

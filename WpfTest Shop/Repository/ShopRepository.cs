﻿using System.Collections.Generic;
using System.Linq;

namespace WpfTest_Shop.Repository
{
    public class ShopRepository
    {
        private ShopContext _context;
        
        public ShopRepository()
        {
            _context = new ShopContext();
        }

        public IEnumerable<Shop> FindAll()
        {
            return _context.Shops;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest_Shop.Repository
{
    class PersonRepository
    {
        private ShopContext _context;

        public PersonRepository()
        {
            _context = new ShopContext();
        }

        public IEnumerable<Person> FindAll()
        {
            return _context.Person;
        }

        public IEnumerable<Person> FindUnemployedPersons()
        {
            var personList = (from p in _context.Person
                              from ps in _context.PersonShop.Where(ps => p.PersonId == ps.PersonId).DefaultIfEmpty()
                              where ps.PersonId == null
                              select p);
            return personList;
        }

        public Person FindByName(String name)
        {
            Person person = _context.Person.SingleOrDefault(p => p.Name == name);
            return person;
        }
    }
}

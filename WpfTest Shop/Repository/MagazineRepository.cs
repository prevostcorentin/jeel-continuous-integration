﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest_Shop.Repository
{
    class MagazineRepository
    {
        private ShopContext _context;

        public MagazineRepository()
        {
            _context = new ShopContext();
        }

        public IEnumerable<Magazine> FindAll()
        {
            return _context.Magazines;
        }

        public Magazine FindByName(String name)
        {
            Magazine magazine = _context.Magazines.SingleOrDefault(m => m.Name == name);
            return magazine;
        }
    }
}

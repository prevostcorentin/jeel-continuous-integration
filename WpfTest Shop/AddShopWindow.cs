﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfTest_Shop
{
    internal class AddShopWindow : ShopWindowBase
    {
        public AddShopWindow() : base()
        {
            MagazinesStackPanel.Visibility = Visibility.Visible;
        }

        protected override void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                City selectedCity = ShopInformation.GetCityFromName(CityCombobox.Text);
                ZipCode zipcode = ShopInformation.GetZipCodeByNumber(ZipCodeComboBox.Text);
                if (IsFormFilled)
                {
                    List<Magazine> magazines = AddedMagazines;

                    Shop addShop = AddShop(magazines);
                    addShop.ZipCode = zipcode;

                    if (IsNewCity(CityCombobox.Text))
                    {
                        selectedCity = CreateNewCity();
                        addShop.ZipCode = selectedCity.ZipCodes[0];
                    }
                    if (IsNewZipCode(CityCombobox.Text, ZipCodeComboBox.Text))
                    {
                        zipcode = CreateZipCode();
                        zipcode.City = selectedCity;
                        addShop.ZipCode = zipcode;
                    }

                    if (selectedCity.Shops == null)
                    {
                        selectedCity.Shops = new List<Shop>();
                    }
                    selectedCity.Shops.Add(addShop);

                    List<int> magazinesQuantity = AddedMagazinesQuantity;
                    int count = 0;
                    Order order = new Order()
                    {
                        OrderDate = DateTime.Now,
                        DeliveryDate = DateTime.Now.AddDays(1)
                    };

                    List<MagazineOrder> magOrderList = new List<MagazineOrder>();
                    foreach (Magazine mag in magazines)
                    {
                        var magOrder = Service.AssociatedOneMagToOneOrder(mag, order, magazinesQuantity[count]);
                        magOrderList.Add(magOrder);
                        count++;
                    }

                    order.ManyMagazineOrders = magOrderList;

                    OrderShop orderShop = Service.AssociatedOneOrdertoOneShop(order, addShop);
                    context.Update(selectedCity);
                    context.AddRange(addShop);
                    context.Add(order);
                    context.Add(orderShop);
                    context.SaveChanges();
                    MessageBox.Show("New shop saved");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You must fill every field correctly.");
                }
            }
        }
    }
}

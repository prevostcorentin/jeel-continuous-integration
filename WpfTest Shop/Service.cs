﻿using System.Collections.Generic;

namespace WpfTest_Shop
{
    static class Service
    {
        public static MagazineOrder AssociatedOneMagToOneOrder(Magazine magazine, Order order, int quantity)
        {
            using (var context = new ShopContext())
            {
                var magOrder = new MagazineOrder
                {
                    Magazine = magazine,
                    MagazineId = magazine.MagazineId,
                    Order = order,
                    OrderId = order.OrderId,
                    Quantity = quantity
                };
                return magOrder;
            }
        }
        
        public static OrderShop AssociatedOneOrdertoOneShop(Order order, Shop shop)
        {
            OrderShop orderShop = new OrderShop
            {
                Order = order,
                OrderId = order.OrderId,
                Shop = shop,
                ShopId = shop.ShopId
            };
            return orderShop;
        }

        public static List<PersonShop> AssociatedManyPersonToOneShop(List<Person> persons, Shop shop)
        {
            List<PersonShop> personShops = new List<PersonShop>();
            foreach (Person person in persons)
            {
                var personShop = new PersonShop { Person = person, PersonId = person.PersonId, Shop = shop, ShopId = shop.ShopId };
                personShops.Add(personShop);
            }
            return personShops;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace WpfTest_Shop
{
    static class ShopInformation
    {
        public static List<ZipCode> GetZipCodes()
        {
            using (var context = new ShopContext())
            {
                var zipcodes = (from z in context.ZipCodes
                                select z).ToList();
                return zipcodes;
            }
        }

        public static ZipCode GetZipCodeByNumber(string zipcodeNumber)
        {
            using (var context = new ShopContext())
            {
                var zipcode = (from z in context.ZipCodes
                               where z.ZipCodeNumber == zipcodeNumber
                                select z).FirstOrDefault();
                return zipcode;
            }
        }

        public static string GetZipcodeNumberByShop(Shop shop)
        {
            using (var context = new ShopContext())
            {
                var zipCode = (from s in context.Shops
                               where s.Name == shop.Name
                               select s.ZipCode.ZipCodeNumber).FirstOrDefault();
                return zipCode;
            }
        }

        public static string GetCityNameByShop(Shop shop)
        {
            using (var context = new ShopContext())
            {
                var city = (from s in context.Shops
                               where s.Name == shop.Name
                               select s.City.Name).FirstOrDefault();
                return city;
            }
        }

        public static City GetCityFromName(string cityName)
        {
            using (var context = new ShopContext())
            {
                var city = (from c in context.City
                            where c.Name == cityName
                            select c).FirstOrDefault();
                return city;
            }
        }

        public static County GetCountyFromCityName(string name)
        {
            using (var context = new ShopContext())
            {
                var county = (from co in context.County
                              join c in context.City
                              on co.CountyId equals c.County.CountyId
                              where c.Name == name
                              select co).FirstOrDefault();
                return county;
            }
        }

        public static District GetDistrictFromCountyName(string name)
        {
            using (var context = new ShopContext())
            {
                var district = (from d in context.District
                                join co in context.County
                                on d.DistrictId equals co.District.DistrictId
                                where co.Name == name
                                select d).FirstOrDefault();
                return district;
            }
        }

        public static Country GetCountryFromDistrictName(string name)
        {
            using (var context = new ShopContext())
            {
                var country = (from c in context.Country
                               join d in context.District
                               on c.CountryId equals d.Country.CountryId
                               where d.Name == name
                               select c).FirstOrDefault();
                return country;
            }
        }

        public static List<City> GetCities()
        {
            using (var context = new ShopContext())
            {
                var cityList = (from c in context.City
                                select c).ToList();
                return cityList;
            }
        }
        public static List<County> GetCounties()
        {
            using (var context = new ShopContext())
            {
                var countiesList = (from co in context.County
                                    select co).ToList();
                return countiesList;
            }
        }

        public static List<District> GetDistricts()
        {
            using (var context = new ShopContext())
            {
                var districtList = (from d in context.District
                                    select d).ToList();
                return districtList;
            }
        }

        public static List<Country> GetCountries()
        {
            using (var context = new ShopContext())
            {
                var countryList = (from ct in context.Country
                                   select ct).ToList();
                return countryList;
            }
        }
    }
}

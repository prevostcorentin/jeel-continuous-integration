﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class AddEditionWindow : Window
    {
        public Magazine SelectedMagazine { get; set; }
        public Edition NewEdition { get; set; }

        public AddEditionWindow()
        {
            InitializeComponent();

            List<Magazine> magazines = DisplayInformation.GetAllMagazinesList();
            MagazineNameComboBox.ItemsSource = magazines;
            DataContext = (Magazine)MagazineNameComboBox.SelectedItem;
            MagazineNameComboBox.SelectionChanged += new SelectionChangedEventHandler(SelectMagazineComboBox_SelectionChanged);

        }

        private void SelectMagazineComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedMagazine = (Magazine)MagazineNameComboBox.SelectedItem;
            NewEdition = Creation.CreateEdition(SelectedMagazine);
            EditionTxtBlock.Text = NewEdition.EditionNumber.ToString();
            DateTxtBlock.Text = NewEdition.PublicationDate.ToString();

        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            
            var context = new ShopContext();
            NewEdition = Creation.CreateEdition(SelectedMagazine);
            context.Update(NewEdition);
            
            if (IsNewEdition(NewEdition))
            {
                Order order = Creation.CreateOrder(NewEdition, SelectedMagazine);
                if (order != null)
                {
                    context.Update(order);
                }

                context.SaveChanges();
                MessageBox.Show("New Edition created");
                this.Close();
            }
            else
            {
                MessageBox.Show("This edition already exists");
            }

        }


        private bool IsNewEdition(Edition edition)
        {
            Magazine selectedMag = (Magazine)MagazineNameComboBox.SelectedItem;
            var editions = DisplayInformation.GetAllEditionsOfAMagazine(selectedMag);

            List<Edition> newEditionVerification = editions.Where(e => e.EditionNumber == edition.EditionNumber).ToList();

            if(newEditionVerification.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void EditionTxtBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}

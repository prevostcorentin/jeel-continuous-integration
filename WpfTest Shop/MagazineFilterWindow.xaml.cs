﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class MagazineFilterWindow : Window
    {
        public MagazineFilterWindow()
        {
            InitializeComponent();
            List<Magazine> magazineList = DisplayInformation.GetAllMagazinesList();
            MagazineListView.ItemsSource = magazineList;
            List<Editor> editorList = DisplayInformation.GetEditors();
            List<string> editorNameList = editorList.Select(e => e.Name).ToList();
            EditorListComboBox.ItemsSource = editorNameList;
            List<int> periodicityList = magazineList.Select(m => m.PeriodicityDays).ToList();
            PeriodicityListComboBox.ItemsSource = periodicityList.Distinct(); 
            List<Shop> shopList = DisplayInformation.DisplayDefaultShop();
            ShopListComboBox.ItemsSource = shopList.Select(s => s.Name).ToList();
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null && item.IsSelected)
            {
                EditMagazineWindow editMagazineWindow = new EditMagazineWindow(this);
                editMagazineWindow.Show();
            }
        }

        private void Research_btn_Click(object sender, RoutedEventArgs e)
        {
            if (EditorListComboBox.SelectedItem != null || PeriodicityListComboBox.SelectedItem != null || ShopListComboBox.SelectedItem != null)
            {
                FilterSelectedDisplay();
            }
            else
            {
                MessageBox.Show("You have to select a filter before!");
            }
        }

        private void FilterSelectedDisplay()
        {
            if (IsThreeFilterSelected())
            {
                string selectedEditorName = EditorListComboBox.Text;
                Editor selectedEditor = DisplayInformation.GetEditorByName(selectedEditorName);
                int selectedPeriodicityNumber = Convert.ToInt32(PeriodicityListComboBox.Text);
                string selectedShopName = ShopListComboBox.Text;
                Shop selectedShop = DisplayInformation.GetShopByName(selectedShopName);
                MagazineListView.ItemsSource = DisplayInformation.GetMagazineListByEditorAndPeriodicityAndShop(selectedEditor, selectedPeriodicityNumber, selectedShop).Distinct(); ;
            }
            else if (IsEditorAndPeriodicitySelected())
            {
                string selectedEditorName = EditorListComboBox.Text;
                Editor selectedEditor = DisplayInformation.GetEditorByName(selectedEditorName);
                int selectedPeriodicityNumber = Convert.ToInt32(PeriodicityListComboBox.Text);
                MagazineListView.ItemsSource = DisplayInformation.GetMagazineListByEditorAndPeriodicity(selectedEditor, selectedPeriodicityNumber).Distinct();
            }
            else if (IsEditorAndShopSelected())
            {
                string selectedEditorName = EditorListComboBox.Text;
                Editor selectedEditor = DisplayInformation.GetEditorByName(selectedEditorName);
                string selectedShopName = ShopListComboBox.Text;
                Shop selectedShop = DisplayInformation.GetShopByName(selectedShopName);
                MagazineListView.ItemsSource = DisplayInformation.GetMagazineListByEditorAndShop(selectedEditor, selectedShop).Distinct();
            }
            else if (IsPeriodictyAndShopSelected())
            {
                int selectedPeriodicityNumber = Convert.ToInt32(PeriodicityListComboBox.Text);
                string selectedShopName = ShopListComboBox.Text;
                Shop selectedShop = DisplayInformation.GetShopByName(selectedShopName);
                MagazineListView.ItemsSource = DisplayInformation.GetMagazineListByPeriodicityAndShop(selectedPeriodicityNumber, selectedShop).Distinct();
            }
            else if (IsOnlyEditorSelected())
            {
                string selectedEditorName = EditorListComboBox.Text;
                Editor selectedEditor = DisplayInformation.GetEditorByName(selectedEditorName);
                MagazineListView.ItemsSource = DisplayInformation.GetMagazineListByEditor(selectedEditor).Distinct();
            }
            else if (IsOnlyPeriodicitySelected())
            {
                int selectedPeriodicityNumber = Convert.ToInt32(PeriodicityListComboBox.Text);
                MagazineListView.ItemsSource = DisplayInformation.GetMagazinesListByPeriodicity(selectedPeriodicityNumber).Distinct();
            }
            else if (IsOnlyShopSelected())
            {
                string selectedShopName = ShopListComboBox.Text;
                Shop selectedShop = DisplayInformation.GetShopByName(selectedShopName);
                MagazineListView.ItemsSource = DisplayInformation.GetMagazinesListByShop(selectedShop).Distinct();
            }
        }

        private bool IsThreeFilterSelected()
        {
            if (EditorListComboBox.SelectedItem != null && PeriodicityListComboBox.SelectedItem != null && ShopListComboBox.SelectedItem != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsEditorAndPeriodicitySelected()
        {
            if (EditorListComboBox.SelectedItem != null && PeriodicityListComboBox.SelectedItem != null && ShopListComboBox.SelectedItem == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsEditorAndShopSelected()
        {
            if (EditorListComboBox.SelectedItem != null && PeriodicityListComboBox.SelectedItem == null && ShopListComboBox.SelectedItem != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsPeriodictyAndShopSelected()
        {
            if (EditorListComboBox.SelectedItem == null && PeriodicityListComboBox.SelectedItem != null && ShopListComboBox.SelectedItem != null)
            {
                return true;
            }
            else
            {
                return false;   
            }
        }

        private bool IsOnlyEditorSelected()
        {
            if (EditorListComboBox.SelectedItem != null && PeriodicityListComboBox.SelectedItem == null && ShopListComboBox.SelectedItem == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsOnlyPeriodicitySelected()
        {
            if (EditorListComboBox.SelectedItem == null && PeriodicityListComboBox.SelectedItem != null && ShopListComboBox.SelectedItem == null)
            {
                return true;
            }
            else { return false; }
        }

        private bool IsOnlyShopSelected()
        {
            if (EditorListComboBox.SelectedItem == null && PeriodicityListComboBox.SelectedItem == null && ShopListComboBox.SelectedItem != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Reset_btn_Click(object sender, RoutedEventArgs e)
        {
            List<Magazine> magazineList = DisplayInformation.GetAllMagazinesList();
            MagazineListView.ItemsSource = magazineList;
            EditorListComboBox.Text = String.Empty;
            PeriodicityListComboBox.Text = String.Empty;
            ShopListComboBox.Text = String.Empty;
        }

        private void Cancel_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}


       
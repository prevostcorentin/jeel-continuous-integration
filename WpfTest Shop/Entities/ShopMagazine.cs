﻿using System;

namespace WpfTest_Shop
{
    public class ShopMagazine
    {
        public Guid ShopMagazineId { get; set; }
        public Guid ShopId { get; set; }
        public Shop Shop { get; set; }
        public Guid MagazineId { get; set; }
        public Magazine Magazine { get; set; }
    }
}
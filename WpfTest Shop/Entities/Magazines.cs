﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace WpfTest_Shop
{
    public class Magazine : INotifyPropertyChanged
    {
        public Guid MagazineId { get; set; }
        private String _name { get; set; }
        public String Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        public Editor Editor { get; set; }
        public ICollection<ShopMagazine> ManyShopMagazines { get; set; }
        public ICollection<MagazineOrder> ManyMagazineOrders { get; set; }
        private int _periodicityDays { get; set; }
        public int PeriodicityDays
        {
            get
            {
                return _periodicityDays;
            }
            set
            {
                _periodicityDays = value;
                OnPropertyChanged("PeriodicityDays");
            }
        }

        public List<Edition> Editions { get; set; } = new List<Edition>();

        public Magazine(string name)
        {
            Name = name;
        }

        public Magazine()
        {

        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public override String ToString()
        {
            return Name;
        }
    }
}
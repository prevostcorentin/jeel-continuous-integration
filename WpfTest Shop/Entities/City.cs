﻿using System;
using System.Collections.Generic;

namespace WpfTest_Shop
{
    public class City
    {
        public Guid CityId { get; set; }

        public string Name { get; set; }

        public List<ZipCode> ZipCodes { get; set; } = new List<ZipCode>();

        public County County { get; set; }

        public ICollection<Shop> Shops { get; set; }
    }
}
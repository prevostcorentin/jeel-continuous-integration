﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfTest_Shop
{
    public class Shop : INotifyPropertyChanged
    {
        public Guid ShopId {get; set; }
        public string Name { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public City City { get; set; }
        public ZipCode ZipCode { get; set; }
        public List<OrderAutomatic> OrderAutomatics { get; set; } = new List<OrderAutomatic>();
        public ICollection<ShopMagazine> ManyShopMagazines { get; set; }
        public ICollection<PersonShop> ManyPersonShops { get; set; } = new List<PersonShop>();
        public ICollection<OrderShop> ManyOrderShops { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public override String ToString()
        {
            return Name;
        }

        
    }

    

}
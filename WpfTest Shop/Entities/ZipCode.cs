﻿using System;

namespace WpfTest_Shop
{
    public class ZipCode
    {
        public Guid ZipCodeId { get; set; }
        public string ZipCodeNumber { get; set; }
        public City City { get; set; }
    }
}

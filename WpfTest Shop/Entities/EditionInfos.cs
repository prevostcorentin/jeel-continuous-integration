﻿namespace WpfTest_Shop
{
    public class EditionInfos
    {
        public EditionInfos() { }

        public string EditionId { get; set; }
        public string EditionNumber { get; set; }
        public string PublicationDate { get; set; }
        public string MagazineName { get; set; }
    }
}
﻿using System;

namespace WpfTest_Shop
{
    public class MagazineOrder
    {
        public Guid MagazineId { get; set; }
        public Magazine Magazine { get; set; }
        public Guid OrderId { get; set; }
        public Order Order { get; set; }
        public int Quantity { get; set; }
    }

}
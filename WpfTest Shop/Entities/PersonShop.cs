﻿using System;

namespace WpfTest_Shop
{
    public class PersonShop
    {
        public Guid PersonShopId { get; set; }
        public Guid PersonId { get; set; }
        public Person Person { get; set; }
        public Guid ShopId { get; set; }
        public Shop Shop { get;set; }
    }
}
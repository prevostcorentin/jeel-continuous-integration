﻿using System;

namespace WpfTest_Shop
{
    public class OrderAutomatic
    {
        public Guid OrderAutomaticId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Magazine Magazine { get; set; }
        public int Quantity { get; set; }
        public Shop Shop { get; set; }
    }
}

﻿using System;

namespace WpfTest_Shop
{
    public class MagazineInfos
    {
        public MagazineInfos() { }

        public string Name { get; set; }
        public string MagazineId { get; set; }
        public string Quantity { get; set; }
        public DateTime DateParutionFirstEdition { get; set; }
        public string PeriodicityDaysMagazine { get; set; }
    }

}



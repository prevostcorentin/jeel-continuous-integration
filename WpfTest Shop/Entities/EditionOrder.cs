﻿using System;

namespace WpfTest_Shop
{
    public class EditionOrder
    {
        public Guid EditionOrderId { get; set; }
        public Guid EditionId { get; set; }
        public Edition Edition { get; set; }
        public Guid OrderId { get; set; }
        public Order Order { get; set; }
        public int Quantity { get; set; }
    }
}

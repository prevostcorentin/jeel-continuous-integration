﻿using System;

namespace WpfTest_Shop
{
    public class Edition
    {
        public Guid EditionId { get; set; }
        public int EditionNumber { get; set; }
        public Magazine Magazine { get; set; }
        public DateTime PublicationDate { get; set; }

        public Edition(int editionNumber, Magazine magazine, DateTime publicationDate)
        {
            EditionNumber = editionNumber;
            Magazine = magazine;
            PublicationDate = publicationDate;
        }

        public Edition(int editionNumber, Magazine magazine)
        {
            EditionNumber = editionNumber;
            Magazine = magazine;
        }

        public Edition(int editionNumber)
        {
            EditionNumber = editionNumber;
        }

        public Edition()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;


namespace WpfTest_Shop
{
    public class Editor
    {
        public Guid EditorId { get; set; }
        public string Name { get; set; }
        public List<Magazine> Magazines { get; set; }
        public Editor()
        {

        }
        public Editor(string name)
        {
            Name = name;
        }

    }
}
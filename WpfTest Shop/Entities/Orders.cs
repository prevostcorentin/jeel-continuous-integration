﻿using System;
using System.Collections.Generic;


namespace WpfTest_Shop
{
    public class Order
    {
        public Guid OrderId { get; set; }
        public ICollection<MagazineOrder> ManyMagazineOrders { get; set; } = new List<MagazineOrder>();

        public DateTime OrderDate { get; set; }

        public DateTime DeliveryDate { get; set; }

        public ICollection<OrderShop> ManyOrderShops { get; set; } = new List<OrderShop>();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace WpfTest_Shop
{
    public partial class ManualSettingsWindows : Window
    {
        private Shop SelectedShop;

        public ManualSettingsWindows(MainWindow owner)
        {
            InitializeComponent();

            List<Magazine> magazineList = DisplayInformation.GetMagazinesList();
            MagazineListComboBox.ItemsSource = magazineList;
            MagazineListCombobox2.ItemsSource = magazineList;
            MagazineListComboBox_Auto.ItemsSource = magazineList;
            MagazineListCombobox2_Auto.ItemsSource = magazineList;
            this.Title = "Quantity to deliver the " + DateTime.Today.AddDays(1).ToString();
            this.Owner = owner;
            DataContext = owner.ShopListView.SelectedItem;
        }

        public ManualSettingsWindows(ShopInfoWindow owner)
        {
            InitializeComponent();

            List<Magazine> magazineList = DisplayInformation.GetMagazinesList();
            MagazineListComboBox.ItemsSource = magazineList;
            MagazineListCombobox2.ItemsSource = magazineList;
            MagazineListComboBox_Auto.ItemsSource = magazineList;
            MagazineListCombobox2_Auto.ItemsSource = magazineList;
            this.Title = "Quantity to deliver the " + DateTime.Today.AddDays(1).ToString();
            this.Owner = owner;
            DataContext = owner.DataContext;
        }

        private void Validate_btn_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ShopContext())
            {
                int quantityTop = GetQuantity(QuantityTextBox.Text);
                int quantityBottom = GetQuantity(QuantityTextBox2.Text);
                List<MagazineOrder> magazinesOrderList = new List<MagazineOrder>();
                Shop shopSelected = (Shop)DataContext;

                if((IsOrderInfoFull(MagazineListComboBox.Text, quantityTop) || IsOrderInfoFull(MagazineListCombobox2.Text, quantityBottom)) && MagazineListComboBox.Text != MagazineListCombobox2.Text)
                { 
                    Order order = new Order();
                    order.OrderDate = DateTime.Now;
                    order.DeliveryDate = DateTime.Now.AddDays(1);
                    OrderShop orderShop = Service.AssociatedOneOrdertoOneShop(order, shopSelected);

                    if (IsOrderInfoFull(MagazineListComboBox.Text, quantityTop))
                    {
                        MagazineOrder magazineOrder1 = Service.AssociatedOneMagToOneOrder((Magazine)MagazineListComboBox.SelectedItem, order, quantityTop);
                        magazinesOrderList.Add(magazineOrder1);

                        ShopMagazine shopMag1 = AddMagazineToShop((Magazine)MagazineListComboBox.SelectedItem);
                        if (shopMag1 != null)
                        {
                            context.Update(shopMag1);
                        }
                    }
                    
                    if(IsOrderInfoFull(MagazineListCombobox2.Text, quantityBottom))
                    {
                        MagazineOrder magazineOrder2 = Service.AssociatedOneMagToOneOrder((Magazine)MagazineListCombobox2.SelectedItem, order, quantityBottom);
                        magazinesOrderList.Add(magazineOrder2);
                        ShopMagazine shopMag2 = AddMagazineToShop((Magazine)MagazineListCombobox2.SelectedItem);
                        if (shopMag2 != null)
                        {
                            context.Update(shopMag2);
                        }
                    }
                    
                    order.ManyMagazineOrders = magazinesOrderList;
                    context.Update(order);
                    context.Update(orderShop);
                    context.SaveChanges();
                    MessageBox.Show("Order validated");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You have to select two different Magazines and/or a quantity of Magazine to order");
                }
            }
        }

        private void Validate_btn_Auto_Click(object sender, RoutedEventArgs e)
        {
            var context = new ShopContext();
            SelectedShop = (Shop)DataContext;
            int quantityTop = GetQuantity(QuantityTextBox_Auto.Text);
            int quantityBottom = GetQuantity(QuantityTextBox2_Auto.Text);

            if ((IsOrderInfoFull(MagazineListComboBox_Auto.Text, quantityTop) || IsOrderInfoFull(MagazineListComboBox_Auto.Text, quantityBottom))
                && MagazineListComboBox.Text != MagazineListCombobox2.Text )
            {
                if (MagazineListComboBox_Auto.SelectedItem != null)
                {
                    OrderAutomatic orderAutomaticTop = OrderMagazineValidation(MagazineListComboBox_Auto.Text, (Magazine)MagazineListComboBox_Auto.SelectedItem, quantityTop);
                    context.Update(orderAutomaticTop);
                }
                if (MagazineListCombobox2_Auto.SelectedItem != null)
                {
                    OrderAutomatic orderAutomaticBottom = OrderMagazineValidation(MagazineListCombobox2_Auto.Text, (Magazine)MagazineListCombobox2_Auto.SelectedItem, quantityBottom);
                    context.Update(orderAutomaticBottom);
                }
                context.SaveChanges();
                MessageBox.Show("Order validated");
                this.Close();
            }
            else
            {
                MessageBox.Show("You have to select two different Magazines and/or a quantity and/or Start/End Date");
            }
        }

        private OrderAutomatic OrderMagazineValidation(string magazineName, Magazine magazine, int quantity)
        {
            OrderAutomatic orderAutomatic = new OrderAutomatic();
            DateTime start = StartDate_DatePicker.SelectedDate.Value.Date;
            DateTime end = EndDate_DatePicker.SelectedDate.Value.Date;

            if (IsOrderInfoFull(magazineName, quantity) && !IsOrderExisting(magazine, start, end) &&
                   StartDate_DatePicker.SelectedDate.HasValue && EndDate_DatePicker.SelectedDate.HasValue)
            {
                orderAutomatic = CreateOrderAutomatic(magazine, quantity);
                SelectedShop.OrderAutomatics.Add(orderAutomatic); 
            }
            return orderAutomatic;
        }

        private int GetQuantity(string quantityBox)
        {
            int quantity = 0;
            if (quantityBox != String.Empty)
            {
                quantity = Convert.ToInt32(quantityBox);
            }
            return quantity;
        }

        private bool IsOrderInfoFull(string comboBox, int quantity)
        {
            if (comboBox != String.Empty && quantity > 0 && quantity <= 200)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public OrderAutomatic CreateOrderAutomatic(Magazine selectedMagazine, int quantity)
        {
            using(var context =new ShopContext())
            {
                Shop shopSelected = (Shop)DataContext;
                var orderAutomatic = new OrderAutomatic()
                {
                    StartDate = StartDate_DatePicker.SelectedDate.Value.Date,
                    EndDate = EndDate_DatePicker.SelectedDate.Value.Date,
                    Quantity = quantity,
                    Magazine = selectedMagazine,
                    Shop = shopSelected
                };

                return orderAutomatic;
            }
        }

        private bool IsOrderExisting(Magazine magazine, DateTime start, DateTime end)
        {
            List<OrderAutomatic> orders = DisplayInformation.GetAllOrderAutomaticFromAMagazine(magazine);

            var order = orders.Where(o => o.StartDate == start && o.EndDate == end).ToList();
            if(order.Count >0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Cancel_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private ShopMagazine AddMagazineToShop(Magazine magSelected)
        {
            List<Magazine> magazines = DisplayInformation.GetMagazinesListByShop((Shop)DataContext);
            var magName = magazines.Select(m => m.Name).ToList();
            Shop shopSelected = (Shop)DataContext;
            using (var context = new ShopContext())
            {
                if (!magName.Contains(magSelected.Name))
                {
                    var magShop = new ShopMagazine()
                    {
                        Magazine = magSelected,
                        MagazineId = magSelected.MagazineId,
                        Shop = shopSelected,
                        ShopId = shopSelected.ShopId
                    };
                    return magShop;
                }
                else
                {
                    return null;
                }
            }
        }

        private void Cancel_btn_Auto_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

